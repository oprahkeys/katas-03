function oneThroughTwenty() {
    const numbers = []
    for (let counter = 1; counter <= 20; counter ++) {
        numbers.push(counter)
    }
    //header
    const newHeader = document.createElement("h2")

    const newText = document.createTextNode("Kata 1");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
  //answer
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
}
  
  oneThroughTwenty()



function evensToTwenty() {
    const numbers = []
    for (let counter = 2; counter <=20; counter += 2) {
        numbers.push(counter)

    }

    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 2");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
  //answer
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
}
evensToTwenty()

function oddsToTwenty() {
    const numbers = []
    for (let counter = 1; counter <=20; counter +=2 ) {
        numbers.push(counter)
    }
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 3");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
  //answer
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
}
    oddsToTwenty()

function multiplesOfFive() {
    const numbers = []
    for (counter = 5; counter <=100; counter +=5 ){
        numbers.push(counter)
    }

    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 4");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
  //answer
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
}
    multiplesOfFive()


function squareNumbers() {
    const numbers = []
    for (counter = 1; counter <=10; counter ++) {
        numbers.push(counter*counter)
    }
    
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 5");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
  //answer
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
}
    squareNumbers()




function countingBackwards() {
    const numbers = []
    for (counter = 20; counter >=1; counter --) {
        numbers.push(counter)
    }

    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 6");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
  //answer
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
}
    countingBackwards()

function evenNumbersBackwards() {
    const numbers = []
    for(counter = 20; counter >=1; counter-=2) {
    numbers.push(counter)
    }
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 7");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
    }
   
    evenNumbersBackwards()


function oddNumbersBackwards() {
    const numbers = []
    for(counter = 21; counter >=1; counter-=2) {
        numbers.push(counter)
        }
        const newHeader = document.createElement("h2")
        const newText = document.createTextNode("Kata 8");
        newHeader.appendChild(newText);
        const destination = document.getElementById("D1");
        destination.appendChild(newHeader);
        const newElement = document.createElement("div")
        const newKataText = document.createTextNode(numbers);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
        }

   oddNumbersBackwards()

function multiplesOfFiveBackwards() {
    const numbers = []
    for(counter = 100; counter >=1; counter-=5) {
        numbers.push(counter)
    }
        const newHeader = document.createElement("h2")
        const newText = document.createTextNode("Kata 9");
        newHeader.appendChild(newText);
        const destination = document.getElementById("D1");
        destination.appendChild(newHeader);
      //answer
        const newElement = document.createElement("div")
        const newKataText = document.createTextNode(numbers);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
    }

 multiplesOfFiveBackwards()

function squareNumbersBackwards() {
    const numbers = []
    for (counter = 10; counter >=1; counter --) {
        numbers.push(counter*counter)
    }
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 10");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
    }
squareNumbersBackwards()



function sampleArray() {
    const store = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    const numbers = []
    for (counter = 0; counter <=store.length; counter ++) {
        numbers.push(store[counter])
    }
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 11");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
    }

   sampleArray ()


   function sampleArrayEven() {
    const store = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    const numbers = []
    for (counter = 0; counter <=store.length; counter ++) {
        if (store[counter]%2===0) {
        numbers.push(store[counter]) 
    } }
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 12");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
    }
    sampleArrayEven()

    function sampleArrayOdd() {
    const store = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    const numbers = []
    for (counter = 0; counter <=store.length; counter ++) {
        if (store[counter]%2!==0) {
        numbers.push(store[counter]) 
    } }
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 13");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
    const newElement = document.createElement("div")
    const newKataText = document.createTextNode(numbers);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
    }
    sampleArrayOdd()


        function sampleArraySquared() {
        const store = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
        const numbers = []
        for (counter = 0; counter <=store.length; counter ++) {
        numbers.push(store[counter]*store[counter]) 
        } 
        const newHeader = document.createElement("h2")
        const newText = document.createTextNode("Kata 14");
        newHeader.appendChild(newText);
        const destination = document.getElementById("D1");
        destination.appendChild(newHeader);
        const newElement = document.createElement("div")
        const newKataText = document.createTextNode(numbers);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
    }
        sampleArraySquared()
    


    function sampleArraySum() {
        let sum = 0 
        for (counter = 1; counter <=20; counter ++) {
        sum += counter
        }
        const newHeader = document.createElement("h2")
        const newText = document.createTextNode("Kata 15");
        newHeader.appendChild(newText);
        const destination = document.getElementById("D1");
        destination.appendChild(newHeader);
        const newElement = document.createElement("div")
        const newKataText = document.createTextNode(sum);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
        }

    sampleArraySum()


    function sampleArraySumArray() {
        const store = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
        let sum = 0
        for (counter = 0; counter < store.length; counter ++) {
            sum = sum + store[counter]
         console.log(sum)
        }
         console.log(sum)
        const newHeader = document.createElement("h2")
        const newText = document.createTextNode("Kata 16");
        newHeader.appendChild(newText);
        const destination = document.getElementById("D1");
        destination.appendChild(newHeader);
        const newElement = document.createElement("div")
        const newKataText = document.createTextNode(sum);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
    }
    sampleArraySumArray()


    const store = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    function sampleArrayLeast() {
    let least = Math.min(...store)
        
        const newHeader = document.createElement("h2")
        const newText = document.createTextNode("Kata 17");
        newHeader.appendChild(newText);
        const destination = document.getElementById("D1");
        destination.appendChild(newHeader);
        const newElement = document.createElement("div")
        let newKataText = document.createTextNode(least);
        newElement.appendChild(newKataText);
        destination.appendChild(newElement);
        }
        sampleArrayLeast()


    
    function sampleArrayGreatest() {
    let greatest = Math.max(...store)
        
    const newHeader = document.createElement("h2")
    const newText = document.createTextNode("Kata 18");
    newHeader.appendChild(newText);
    const destination = document.getElementById("D1");
    destination.appendChild(newHeader);
    const newElement = document.createElement("div")
    let newKataText = document.createTextNode(greatest);
    newElement.appendChild(newKataText);
    destination.appendChild(newElement);
    }
    sampleArrayGreatest()
    
      


 
    